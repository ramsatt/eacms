import { Injectable, Output } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  @Output() appCreated = new Subject();
  selectedAppl: any;
  constructor() { }

  setSelectedAppl(value: any) {
    this.selectedAppl = value;
  }

  getSelectedAppl() {
    return this.selectedAppl;
  }
}
