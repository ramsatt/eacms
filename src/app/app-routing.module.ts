import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AllAppealsComponent} from './all-appeals/all-appeals.component';
import {CreateAppealComponent} from './create-appeal/create-appeal.component';
import {AppealIntakeComponent} from './appeal-intake/appeal-intake.component';
import {CreateAppellantComponent} from "./create-appellant/create-appellant.component";

const routes: Routes = [
  {path: 'home', component: AllAppealsComponent},
  {path: 'createappeal', component: CreateAppealComponent},
  {path: 'appealintake', component: AppealIntakeComponent},
  {path: 'create-appellant', component: CreateAppellantComponent},
  {path: '', redirectTo: 'home', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
