export interface appellantElement {
  primaryContact: boolean;
  personType: string;
  unableToContact: boolean;
  numberOfAppellant: any;
  firstName: string;
  middleName: string;
  lastName: string;
  relationShipToAppellant: string;
  sufix: any;
  employerName: any;
  dateOfBirth: any;
  gender: any;
  signatureIndicator: any;
  dateSigned: string;
  address: any [];
  email: any[];
  telephoneNumbers: any[];
  identifications: any[];
}
