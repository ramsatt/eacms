import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppealIntakeComponent } from './appeal-intake.component';

describe('AppealIntakeComponent', () => {
  let component: AppealIntakeComponent;
  let fixture: ComponentFixture<AppealIntakeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppealIntakeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppealIntakeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
