import {AfterViewInit, Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, FormControl} from '@angular/forms';
import {SharedService} from '../services/shared.service';
import {animate, state, style, transition, trigger} from "@angular/animations";
import {US_STATES} from "../constants/us-states";
import {COUNTRY_LIST} from "../constants/country-list";
import {appellantElement} from "../model/appellant-model";

declare var $: any;

const ELEMENT_DATA: appellantElement[] = [
  {
    primaryContact: true,
    personType: 'Appellant',
    unableToContact: true,
    numberOfAppellant: 1,
    firstName: 'FRANKIE',
    middleName: 'R',
    lastName: 'Attakai',
    relationShipToAppellant: 'Self',
    sufix: 'Mr',
    employerName: 'CTS',
    dateOfBirth: '',
    gender: 'Male',
    signatureIndicator: true,
    dateSigned: '',
    address: [{
      addressType: 'Mailing',
      streetAddress1: '100 Wall Street',
      streetAddress2: '',
      city: 'New York',
      zipCode: '17366',
      country: 'USA',
      state: 'New York',
    }],
    email: [],
    telephoneNumbers: [
      {
        textPreference: true,
        phoneType: 'Primary',
        telephoneNumber: '90890987',
        extension: '01'
      }
    ],
    identifications: []
  },
  {
    primaryContact: true,
    personType: 'Appellant',
    unableToContact: true,
    numberOfAppellant: 1,
    firstName: 'Test',
    middleName: 'R',
    lastName: 'Attakai',
    relationShipToAppellant: 'Self',
    sufix: 'Mr',
    employerName: 'CTS',
    dateOfBirth: '',
    gender: 'Male',
    signatureIndicator: true,
    dateSigned: '',
    address: [{
      addressType: 'Mailing',
      streetAddress1: '100 Wall Street',
      streetAddress2: '',
      city: 'New York',
      zipCode: '17366',
      country: 'USA',
      state: 'New York',
    }],
    email: [],
    telephoneNumbers: [
      {
        textPreference: true,
        phoneType: 'Primary',
        telephoneNumber: '90890987',
        extension: '01'
      }
    ],
    identifications: []
  }
]

@Component({
  selector: 'app-appeal-intake',
  templateUrl: './appeal-intake.component.html',
  styleUrls: ['./appeal-intake.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', visibility: 'hidden'})),
      state('expanded', style({height: '*', visibility: 'visible'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class AppealIntakeComponent implements OnInit, AfterViewInit {
  selectedIndex: any;
  dataSource: any[];
  expandedElement: any;
  usStates = US_STATES;
  countryListArray = COUNTRY_LIST;
  isAddAppellant = false;
  appealIntakeDetails !: FormGroup;
  panelOpenState = false;
  intaketypes: any[] = [
    {name: 'Appeal'},
    {name: 'Return Email'},
    {name: 'Return Mail'},
    {name: 'State Information Exchange'},
    {name: 'Supporting Documentation'},
    {name: 'Supporting Information'},
  ];
  options: FormGroup | undefined;
  hideRequiredControl = new FormControl(false);
  floatLabelControl = new FormControl('auto');


  receipttypes: any[] = [
    {name: 'Business Email'},
    {name: 'Business Phone'},
    {name: 'Fax'},
    {name: 'Home Phone'},
    {name: 'Mail'},
    {name: 'Mobile'},
    {name: 'On-Line'},
    {name: 'Personal Email'},

  ];

  submittertypes: any[] = [
    {name: 'Appellant'},
    {name: 'Agent'},
    {name: 'Authorized Rep'},
    {name: 'Agency'},
    {name: 'Advocate'},
    {name: 'Parent'},

  ];

  pendedReasonTypes: any[] = [
    {name: 'Appellant Request to Pend'},
    {name: 'Disaster Extension'},
    {name: 'Escalated to MAG for direcyion'},
  ];

  appl: any;
  // @ts-ignore
  appellantAddressFrom: FormGroup;

  constructor(private fb: FormBuilder,
              private shredService: SharedService) {
    this.options = fb.group({
      hideRequired: this.hideRequiredControl,
      floatLabel: this.floatLabelControl,
    });
    if(localStorage.getItem('dataSource')) {
      // @ts-ignore
      this.dataSource = JSON.parse(localStorage.getItem('dataSource'));
    } else {
      this.dataSource = ELEMENT_DATA;
    }
  }

  ngOnInit(): void {
    this.appealIntakeDetails = new FormGroup({
      aplintakeType: new FormControl(),
      aplreceiptType: new FormControl(),
      aplsubmittedBy: new FormControl(),
      pendedReason: new FormControl(),
      aplRequestDate: new FormControl()
    });
    this.appl = this.shredService.getSelectedAppl();
    console.log(this.appl);
    this.createAppellantAddressFrom();
  }

  isExpansionDetailRow = (i: number, row: Object) => row.hasOwnProperty('detailRow');

  ngAfterViewInit(): void {
    setTimeout(() => {
      $('.accordion-toggle').click(() => {
        console.log('qq');
        $('.hiddenRow').show();
      });
    }, 2000)
  }

  addAddress() {
    $('#addAddress').modal('show');
  }

  createAppellantAddressFrom() {
    this.appellantAddressFrom = this.fb.group({
      addressType: [''],
      streetAddress1: [''],
      streetAddress2: [''],
      city: [''],
      zipCode: [''],
      country: [''],
      state: [''],

    });
  }

  saveAddress() {
    this.dataSource[this.selectedIndex].address.push(this.appellantAddressFrom.getRawValue());
    //http
    this.appellantAddressFrom.reset();
    $('#addAddress').modal('hide');
  }

  selectAppellant(appellant: any) {
    let index = this.dataSource.indexOf(appellant);
    if (this.selectedIndex == index) {
      this.selectedIndex = undefined;
    } else {
      this.selectedIndex = index;
    }
  }

  addAppellantForm() {
    this.isAddAppellant = true;
    this.selectedIndex = undefined;
  }

  createAppellant(event: any) {
    this.dataSource.push(event);
    localStorage.setItem('dataSource', JSON.stringify(this.dataSource));
    this.isAddAppellant = false;
  }

}
