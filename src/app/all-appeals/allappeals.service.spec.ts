import { TestBed } from '@angular/core/testing';

import { AllappealsService } from './allappeals.service';

describe('AllappealsService', () => {
  let service: AllappealsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AllappealsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
