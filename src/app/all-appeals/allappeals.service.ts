import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
import { Appeal } from '../model/appeal';
import { AllAppeals } from '../model/all-appeals';

@Injectable({
  providedIn: 'root'
})
export class AllappealsService {

  getAllAppealsUrl :  string;
  constructor(private http : HttpClient) { 
    //this.addAppealUrl = "http://localhost:9091/contat/addContact";
     this.getAllAppealsUrl = "http://localhost:8089/task/getAll";
  
}


getAllAppeals(): Observable<AllAppeals[]>{
  // console.log(apl);
  return this.http.get<AllAppeals[]>(this.getAllAppealsUrl);
}

}
