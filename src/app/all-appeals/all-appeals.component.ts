import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { AllAppeals } from '../model/all-appeals';
import { CreateappealService } from 'src/app/create-appeal/createappeal.service';
import { AppealIntakeComponent } from 'src/app/appeal-intake/appeal-intake.component';
import { SharedService } from '../services/shared.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-all-appeals',
  templateUrl: './all-appeals.component.html',
  styleUrls: ['./all-appeals.component.css']
})
export class AllAppealsComponent implements OnInit {

  appList : AllAppeals[] = [];

  constructor(private aplService : CreateappealService, private sharedService : SharedService,
    private router: Router) {
    this.sharedService.appCreated.subscribe(data => {
      console.log(data);
      this.getAllAppeals();
    });
   }

    // ngOnChanges(changes: SimpleChanges): void {
  //   throw new Error('Method not implemented.');
  // }

  ngOnInit(): void {
    this.getAllAppeals();
    // this.sharedService.appCreated.subscribe(data => {
    //   this.getAllAppeals();
    // });
  }

  getAllAppeals(){
    this.aplService.getAllAppeals().subscribe(res=>{
      this.appList = res;
      console.log("getallappeallist --> ",res);
    }, err=>{
      console.log("ERROR WHILE FETCHING ALL APPEALS");
    });
  }

  selectAppl(appl: any) {
    this.sharedService.setSelectedAppl(appl);
    this.router.navigate(['/appealintake']);
  }


}
