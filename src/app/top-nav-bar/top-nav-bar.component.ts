import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { CreateAppealComponent } from '../create-appeal/create-appeal.component';
import { CreateappealService } from '../create-appeal/createappeal.service';

@Component({
  selector: 'app-top-nav-bar',
  templateUrl: './top-nav-bar.component.html',
  styleUrls: ['./top-nav-bar.component.css']
})
export class TopNavBarComponent implements OnInit {
  appList: any = [];
  @Output() createAppealEvent: EventEmitter<any> = new EventEmitter();

  constructor(public dialog: MatDialog,
    private aplService : CreateappealService) { }
  openCreateAplDialog() {
    const dialogRef = this.dialog.open(CreateAppealComponent, { disableClose: true });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      this.createAppealEvent.emit();
    });
  }

  ngOnInit(): void {

  }


}
