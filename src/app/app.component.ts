import { Component, OnInit } from '@angular/core';
import { CreateappealService } from 'src/app/create-appeal/createappeal.service';
import { Appeal } from './model/appeal';
import { AllAppeals } from './model/all-appeals';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'eacms-third-poc';
  appList : AllAppeals[] = []

  constructor(private aplService : CreateappealService) { }

  ngOnInit(): void {
    this.getAllAppeals();
  }

  onCreateAppealEvent(event: any) {
    console.log("App received event: ", event);
    //this.getAllAppeals();
  }

  getAllAppeals(){
    this.aplService.getAllAppeals().subscribe(res=>{
      this.appList = res;
      console.log("getallappeallist --> ",res);
    }, err=>{
      console.log("ERROR WHILE FETCHING ALL APPEALS");
    });
  }
}
