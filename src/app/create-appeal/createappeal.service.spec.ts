import { TestBed } from '@angular/core/testing';

import { CreateappealService } from './createappeal.service';

describe('CreateappealService', () => {
  let service: CreateappealService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CreateappealService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
