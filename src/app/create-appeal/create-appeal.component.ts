import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Appeal } from 'src/app/model/appeal';
import { AllAppeals } from 'src/app/model/all-appeals';
import { CreateappealService } from 'src/app/create-appeal/createappeal.service';
import { Injector } from "@angular/core";
import { SharedService } from '../services/shared.service';

@Component({
  selector: 'app-create-appeal',
  templateUrl: './create-appeal.component.html',
  styleUrls: ['./create-appeal.component.css']
})
export class CreateAppealComponent implements OnInit {
  appealDetails !: FormGroup;
  appObj : Appeal = new Appeal();
  appList : AllAppeals[] = [];
  applRespObject : any;
//  toastr : ToastrService;
//  injector : Injector;

  apltypes: any[] = [
    { name: 'Individual' },
    { name: 'Large Employer' },
    { name: 'SHOP Employee' },
    { name: 'SHOP Employer' },

];



  constructor(private fb: FormBuilder,private aplService : CreateappealService,
    private dialogRef: MatDialogRef<CreateAppealComponent>,
    private sharedService: SharedService) {


  }

  ngOnInit(): void {

    this.appealDetails = this.fb.group({
      aplFirstName : [''],
      aplMiddleName : [''],
      aplLastName : [''],
      aplDateofBirth:[''],
      aplType:['']
    });
  }

  createappeal() {

      //console.log(this.appealDetails);
      this.appObj.aplFirstName = this.appealDetails.value.aplFirstName;
      this.appObj.aplMiddleName = this.appealDetails.value.aplMiddleName;
      this.appObj.aplLastName = this.appealDetails.value.aplLastName;
      this.appObj.aplDateofBirth = this.appealDetails.value.aplDateofBirth;
      this.appObj.aplType = this.appealDetails.value.aplType;
      console.log(this.appObj);

      this.aplService.addAppeal(this.appObj).subscribe(res=>{
        console.log("NoExceptionaddappeal --> "+res);
        this.sharedService.appCreated.next(true);
       this.applRespObject = res;
       console.log("appealobjectfromresponse" +this.applRespObject);
       //this.toastr.success(_.get(data, 'message', 'Task Completed Successfully'));
      }, err=>{
        console.log("Exception --> "+err);
      });


    this.dialogRef.close(this.appealDetails.value);

    //service call
  }

}
