import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
import { Appeal } from '../model/appeal';
import { AllAppeals } from '../model/all-appeals';

@Injectable({
  providedIn: 'root'
})
export class CreateappealService {

  addAppealUrl : string;
  getAllAppealsUrl :  string;
  constructor(private http : HttpClient) { 
    //this.addAppealUrl = "http://localhost:9091/contat/addContact";
    this.addAppealUrl = "http://localhost:8089/process/start?processDefinitionKey=Process_loan_application_service";
    this.getAllAppealsUrl = "http://localhost:8089/task/getAll";
  
}

addAppeal(apl : Appeal){
  console.log(apl);
  return this.http.post(this.addAppealUrl, apl);
}

getAllAppeals(): Observable<AllAppeals[]>{
  // console.log(apl);
  return this.http.get<AllAppeals[]>(this.getAllAppealsUrl);
}

}
