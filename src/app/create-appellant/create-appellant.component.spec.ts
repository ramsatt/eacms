import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAppellantComponent } from './create-appellant.component';

describe('CreateAppellantComponent', () => {
  let component: CreateAppellantComponent;
  let fixture: ComponentFixture<CreateAppellantComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateAppellantComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAppellantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
