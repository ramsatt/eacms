import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {US_STATES} from "../constants/us-states";
import {COUNTRY_LIST} from "../constants/country-list";
declare var $: any;
@Component({
  selector: 'app-create-appellant',
  templateUrl: './create-appellant.component.html',
  styleUrls: ['./create-appellant.component.css']
})
export class CreateAppellantComponent implements OnInit {
  // @ts-ignore
  appellantAddressFrom: FormGroup;
  // @ts-ignore
  telephoneForm: FormGroup;
  // @ts-ignore
  emailFrom: FormGroup;
  // @ts-ignore
  appellantForm: FormGroup;
  usStates = US_STATES;
  countryListArray = COUNTRY_LIST;
  addressArray: any[] = [];
  telePhoneNumbersArray: any[] = [];
  isAddPhone = false;
  isUpdatePhone = false;
  editTelephoneIndex: any;
  emailArray: any[] = [];
  isAddEmail = false;
  isUpdateEmail = false;
  editEmailIndex: any;
  appellantInfo: any;
  identificationArray: any[] = [];
  @Output() createAppellantInfo = new EventEmitter();
  constructor(private fb: FormBuilder) {
    this.createAppellantAddressFrom();
    this.createTelephoneForm();
    this.createEmailFrom();
    this.createAppellantForm();
  }

  ngOnInit(): void {
  }

  addAddress() {
    $('#newAddress').appendTo("body").modal('show');
  }

  closeModal() {
    $('#newAddress').modal('hide');
  }

  createAppellantForm() {
    this.appellantForm = this.fb.group({
      primaryContact: [''],
      personType: [''],
      unableToContact: [''],
      numberOfAppellant: [''],
      firstName: [''],
      middleName: [''],
      lastName: [''],
      sufix: [''],
      employerName: [''],
      dateOfBirth: [''],
      gender: [''],
      relationshipToAppellant: [''],
      signatureIndicator: [''],
      dateSigned: ['']
    });
  }

  createAppellantAddressFrom() {
    this.appellantAddressFrom = this.fb.group({
      addressType: [''],
      streetAddress1: [''],
      streetAddress2: [''],
      city: [''],
      zipCode: [''],
      country: [''],
      state: ['']
    });
  }

  createTelephoneForm() {
    this.telephoneForm = this.fb.group({
      textPreference: [''],
      phoneType: [''],
      telephoneNumber: [''],
      extension: ['']
    });
  }

  createEmailFrom() {
    this.emailFrom = this.fb.group({
      emailPreference: [''],
      emailType: [''],
      emailAddress: ['']

    });
  }

  saveAddress() {
    this.addressArray.push(this.appellantAddressFrom.getRawValue());
    this.appellantAddressFrom.reset();
    $('#newAddress').modal('hide');
  }

  addPhone() {
    this.isAddPhone = true;
  }

  saveTelephone() {
    this.telePhoneNumbersArray.push(this.telephoneForm.getRawValue());
    this.telephoneForm.reset();
    this.isAddPhone = false;
  }

  editTelephoneDetails(phoneDetails: any) {
    if(phoneDetails) {
      this.editTelephoneIndex = this.telePhoneNumbersArray.indexOf(phoneDetails);
      this.telephoneForm.controls['textPreference'].setValue(phoneDetails.textPreference);
      this.telephoneForm.controls['phoneType'].setValue(phoneDetails.phoneType);
      this.telephoneForm.controls['telephoneNumber'].setValue(phoneDetails.telephoneNumber);
      this.telephoneForm.controls['extension'].setValue(phoneDetails.extension);
    }
    this.isAddPhone = true;
    this.isUpdatePhone = true;
  }

  updateTelephone() {
    this.telePhoneNumbersArray[this.editTelephoneIndex] = this.telephoneForm.getRawValue();
    this.isAddPhone = false;
    this.isUpdatePhone = false;
    this.telephoneForm.reset();
  }

  deleteTelephoneNumber(phoneDetails: any) {
    let deleteIndex = this.telePhoneNumbersArray.indexOf(phoneDetails);
    if (deleteIndex > -1) {
      this.telePhoneNumbersArray.splice(deleteIndex, 1);
    }
  }

  //email
  addEmail() {
    this.isAddEmail = true;
  }

  saveEmail() {
    this.emailArray.push(this.emailFrom.getRawValue());
    this.isAddEmail = false;
    this.emailFrom.reset();
  }

  updateEmail() {
    this.emailArray[this.editEmailIndex] = this.emailFrom.getRawValue();
    this.isAddPhone = false;
    this.isUpdatePhone = false;
    this.emailFrom.reset();
  }

  editEmail(emailDetails: any) {
    if(emailDetails) {
      this.editEmailIndex = this.emailArray.indexOf(emailDetails);
      this.emailFrom.controls['emailPreference'].setValue(emailDetails.emailPreference);
      this.emailFrom.controls['emailType'].setValue(emailDetails.emailType);
      this.emailFrom.controls['emailAddress'].setValue(emailDetails.emailAddress);
    }
    this.isUpdateEmail = true;
    this.isAddEmail = true;
  }

  deleteEmail(emailDetails: any) {
    let deleteIndex = this.emailArray.indexOf(emailDetails);
    if (deleteIndex > -1) {
      this.emailArray.splice(deleteIndex, 1);
    }
  }

  createAppellant() {
    this.appellantInfo = this.appellantForm.getRawValue();
    this.appellantInfo.address = this.addressArray;
    this.appellantInfo.email = this.emailArray;
    this.appellantInfo.telephoneNumbers = this.telePhoneNumbersArray;
    this.appellantInfo.identifications = this.identificationArray;
    console.log(this.appellantInfo);
    this.createAppellantInfo.emit(this.appellantInfo);
  }


}
